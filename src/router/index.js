import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Products from '../views/Products.vue'
import Albums from '../views/Albums.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/products',
    name: 'Products',
    component: Products
  },
  {
    path: '/albums',
    name: 'Albums',
    component: Albums
  },
  {
    path: '/user',
    name: 'User',
    component: () => import(/* webpackChunkName: "about" */ '../views/User.vue'),
    children: [
        {
            path: ':id',
            name: 'View user',
            component: () => import('../views/User.vue'),
        }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
