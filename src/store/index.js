import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger';
import Users from '@/store/modules/Users';
import Products from '@/store/modules/Products';
import Comments from '@/store/modules/Comments';
import Albums from '@/store/modules/Albums';
import Search from '@/store/modules/Search';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        Users: Users,
        Products: Products,
        Comments: Comments,
        Albums: Albums,
        Search: Search
    },
    strict: debug,
    plugins: debug? [createLogger()] : [],
})
