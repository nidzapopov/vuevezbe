import Users from '@/services/Users/Users.js';

export default{
    namespaced: false,
    /**
     * Deklarisanje promenljivih
     */
	state: {
        users: [],
        user: null
    },
    /**
     * Menjanje vrednosti nad promenljivim
     */
	mutations: {
        SET_USERS(state, data){
            state.users = data;
        },
        SET_USER(state, data){
            state.user = data;
        },
        ADD_USER(state, data){
            state.users.push(data);
        },
        UPDATE_USER(state, data){
            state.users.splice(data.index, 1);
            state.users.push(data.model);
        },
    },
    /**
     * Standardna metoda za recimo pozivanje podataka preko axios-a ili akcija gde ce se trigerovati neka mutacija
     */
	actions: {
        async getAllusers({commit}){
            return await Users.getAllusers().then(response => {
                commit('SET_USERS', response.data);
                return response;
            });
        },

        async getUser({commit}, obj){ 
            return await Users.getUser(obj).then(response => {
                commit('SET_USER', response.data);
                return response;
            });
        },

        async addUser({commit, state}, obj){ 
            obj.id = state.users.length + 1;
            commit('SET_USER', obj);
            commit('ADD_USER', obj);
            return await Promise.resolve(obj); 
        },

        async updateUser({commit, state}, obj){ 
            const index = state.users.findIndex( element => element.id === obj.id);
            obj.model.id = obj.id;
            console.log(index, obj.model)
            commit('UPDATE_USER', {model: obj.model, index: index});
        },
    },
    /**
     * pozivanje promenljivih iz komponenti
     */
    getters: {
        getUser(state) {
            return state.user;
        }
    }
};