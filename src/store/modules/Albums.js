import Albums from '@/services/Albums/Albums.js';

export default{
    namespaced: false,
    /**
     * Deklarisanje promenljivih
     */
	state: {
        albums: [],
        photos: [],
    },
    /**
     * Menjanje vrednosti nad promenljivim
     */
	mutations: {
        SET_ALBUMS(state, data){
            state.albums = data;
        },
        SET_PHOTOS(state, data){
            state.photos = data;
        },
    },
    /**
     * Standardna metoda za recimo pozivanje podataka preko axios-a ili akcija gde ce se trigerovati neka mutacija
     */
	actions: {
        async getAlbums({commit}){
            return await Albums.getAlbums().then(response => {
                commit('SET_ALBUMS', response.data);
                return response;
            });
        },
        async getPhotos({commit}){
            return await Albums.getPhotos().then(response => {
                commit('SET_PHOTOS', response.data);
                return response;
            });
        },
    },
    /**
     * pozivanje promenljivih iz komponenti
     */
    getters: {
        
    }
};