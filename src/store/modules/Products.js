import Products from '@/services/Products/Products.js';

export default{
    namespaced: false,
    /**
     * Deklarisanje promenljivih
     */
	state: {
        products: [],
        someProducts: [],
        count: 0,
        curentPage: 1,
        pages: 0,
        perPage: 5,
        product: null,
        productsPerPage: [],
        observer: []
    },
    /**
     * Menjanje vrednosti nad promenljivim
     */
	mutations: {
        SET_PRODUCTS(state, data){
            state.products = data;
        },
        SET_SOME_PRODUCTS(state, data){
            for(let i=0;i<10;i++) {
                state.someProducts.push(data[i]);
            }
        },
        SET_PRODUCTS_PER_PAGE(state, data){
            state.productsPerPage = data;
        },
        SET_PRODUCT(state, data){
            state.product = data;
        },
        SET_COUNT(state, data){
            state.count = data;
        },
        SET_PAGES(state, data){
            const sum = data.length / state.perPage;
            state.pages = sum.toFixed();
        },
        SET_CURENT_PAGE(state, data){
            state.curentPage = data;
        },
        SET_OBSERVER(state, data){
            state.observer = data;
        },
    },
    /**
     * Standardna metoda za recimo pozivanje podataka preko axios-a ili akcija gde ce se trigerovati neka mutacija
     */
	actions: {
        async getProducts({commit, dispatch}){
            return await Products.getProducts().then(response => {
                commit('SET_PRODUCTS', response.data);
                commit('SET_SOME_PRODUCTS', response.data);
                dispatch('setDataForListing', response.data);
                return response;
            });
        },

        setDataForListing({commit, dispatch}, data) {
            commit('SET_COUNT', data.length);
            commit('SET_PAGES', data);
            commit('SET_OBSERVER', data);
            dispatch('filterProducts', data)
        },

        filterProducts({commit, state}, data) {
            const arr = [];
            const to = state.curentPage * state.perPage;
            const from = to - state.perPage;
            for(let i=from;i<to;i++) {
                if(data[i] !== undefined) {
                    (data.length !== 0) 
                        ? arr.push(data[i])
                        : arr.push(state.products[i]);
                }
            }
            commit('SET_PRODUCTS_PER_PAGE', arr);
        },

        changePage({commit, dispatch, state}, index) {
            commit('SET_CURENT_PAGE', index);
            dispatch('filterProducts', state.observer);
        },
        
        setFilterByUser({dispatch, state, commit}, id) {
            const products = state.products;
            const arr = products.filter( element => element.userId === id);
            commit('SET_CURENT_PAGE', 1);
            dispatch('setDataForListing', arr);
        },

        setFilterByAllProducts({dispatch, state, commit}) {
            commit('SET_CURENT_PAGE', 1);
            dispatch('setDataForListing', state.products);
        }
    },
    /**
     * pozivanje promenljivih iz komponenti
     */
    getters: {
        getProducts(state) {
            return state.products;
        },
        getSomeProducts(state) {
            return state.someProducts;
        }
    }
};