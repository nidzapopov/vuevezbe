import Comments from '@/services/Comments/Comments.js';

export default{
    namespaced: false,
    /**
     * Deklarisanje promenljivih
     */
	state: {
        comments: [],
        someComments: [],
    },
    /**
     * Menjanje vrednosti nad promenljivim
     */
	mutations: {
        SET_COMMENTS(state, data){
            state.comments = data;
        },
        SET_SOME_COMMENTS(state, data){
            for(let i=0;i<10;i++) {
                state.someComments.push(data[i]);
            }
        },
    },
    /**
     * Standardna metoda za recimo pozivanje podataka preko axios-a ili akcija gde ce se trigerovati neka mutacija
     */
	actions: {
        async getComments({commit}){
            return await Comments.getComments().then(response => {
                commit('SET_COMMENTS', response.data);
                commit('SET_SOME_COMMENTS', response.data);
                return response;
            });
        },
    },
    /**
     * pozivanje promenljivih iz komponenti
     */
    getters: {
        getComments(state) {
            return state.comments;
        },
        getSomeComments(state) {
            return state.someComments;
        }
    }
};