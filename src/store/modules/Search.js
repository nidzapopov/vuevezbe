import Search from '@/services/Search/Search.js';

export default{
    namespaced: false,
    /**
     * Deklarisanje promenljivih
     */
	state: {
        searchList: []
    },
    /**
     * Menjanje vrednosti nad promenljivim
     */
	mutations: {
        SET_LIST(state, data) {
            state.searchList = data;
        },
        REMOVE_LIST(state) {
            state.searchList = [];
        },
    },
    /**
     * Standardna metoda za recimo pozivanje podataka preko axios-a ili akcija gde ce se trigerovati neka mutacija
     */
	actions: {
        async search({commit}, val){
            return await Search.search(val).then(response => {
                commit('SET_LIST', response.data);
                return response;
            });
        },
        removeSearch({commit}){
            commit('REMOVE_LIST');
        },
    },
    /**
     * pozivanje promenljivih iz komponenti
     */
    getters: {
        
    }
};