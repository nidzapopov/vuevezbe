import Api from '@/services/Axios.js';

export default {
    getAlbums(){
        return Api.get('albums');
    },
    getPhotos(){
        return Api.get('photos');
    },
};
