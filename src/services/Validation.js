const validation = {};
let error = [];

validation.isNumber = (val, prop) => {
    const numbers = /^[0-9]+$/;
    if(!val.match(numbers)) error.push(`<b>${prop}</b>: mora biti broj`);
}

validation.isEmpty = (val, prop) => {
    if(val === '') error.push(`<b>${prop}</b>: mora biti popunjen`);
}

validation.minMax = (val, prop, arr) => {
    if(val < arr[0] || val > arr[1]) error.push(`<b>${prop}</b>: moze biti izmedju ${arr[0]} - ${arr[1]}`);
}

validation.setValidation = (validationObj, model) => {
    error = [];
    for(let key in validationObj) {
        const val = model[key];
        validation.checkObject(val, key, validationObj[key]);
    }
    return error;
}

validation.checkObject = (val, prop, obj) => { 
    for(let key in obj) {
        switch (key) {
            case 'minMax':
                if(obj[key]) validation.minMax(val, prop, obj[key]);
            break;
            case 'number':
                if(obj[key]) validation.isNumber(val, prop);
                break;
            case 'require':
                if(obj[key]) validation.isEmpty(val, prop);
            break;
            default:
                break;
        }
    }
}

export default validation;