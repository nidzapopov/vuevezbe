import Axios from 'axios';

const axios = Axios.create({
	baseURL: 'https://jsonplaceholder.typicode.com/'
});

axios.interceptors.request.use(config => {
	return config;
});

axios.interceptors.response.use(response => {
    console.log(response)
	return response;
});

export default axios;