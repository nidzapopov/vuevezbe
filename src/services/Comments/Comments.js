import Api from '@/services/Axios.js';

export default {
    getComments(){
        return Api.get('comments');
    },
};
