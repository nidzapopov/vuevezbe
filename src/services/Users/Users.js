import Api from '@/services/Axios.js';

export default {
    getAllusers(){
        return Api.get('users');
    },
    getUser(obj){
        return Api.get('users/'+obj.id);
    },
};
