import Api from '@/services/Axios.js';

export default {
    search(val){
        return Api.get('albums/'+val+'/photos');
    },
};
